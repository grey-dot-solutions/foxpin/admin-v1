/* eslint-disable */
import Document, { Html, Head, Main, NextScript } from 'next/document';

class CustomDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          {/* FOR META TAGS & SEO */}
          <meta name="Property" content="Custom Property Value" />
        </Head>

        <body>
          <Main />
        </body>

        <NextScript />
      </Html>
    );
  }
}

export default CustomDocument;
