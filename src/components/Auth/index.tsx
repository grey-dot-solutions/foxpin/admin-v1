import PropTypes from 'prop-types';
import { useState } from 'react';
import { AuthContext } from '../../utils/context';

const Auth = ({ children }) => {
  const [user, setUser] = useState({
    isAuthenticated: false,
    fullName: '',
    firstName: '',
    middleName: '',
    lastName: '',
    role: '',
  });

  // @TODO: FOR TESTING PURPOSES ONLY. THIS SETS USER AFTER 2 SECONDS.
  setTimeout(() => {
    setUser({
      isAuthenticated: true,
      fullName: 'Bhupen Pal',
      firstName: 'Bhupen',
      middleName: '',
      lastName: 'Pal',
      role: 'admin',
    });
  }, 2000);

  return (
    <AuthContext.Provider
      value={user}
    >
      {children}
    </AuthContext.Provider>
  );
};

// DEFINING PROP TYPES AND DEFAULT PROPS
Auth.propTypes = {
  children: PropTypes.node,
};

Auth.defaultProps = {
  children: null,
};

export default Auth;
