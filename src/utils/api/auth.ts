import axios from '../axios';

export const adminLogin = async (req: any) => {
  const user = await axios.post('/auth/signin', {
    email: req.email,
    password: req.password,
  });

  return user.data;
};

export const admingLogout = async () => {
  await axios.delete('/logout');
};
